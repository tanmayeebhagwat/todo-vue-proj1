import firebase from "firebase/app"
import "firebase/firestore"

//initialize firebase
const firebaseConfig = {
    apiKey: "AIzaSyAa90XkYfHjLeDDU9VQibeQj_BzZc_q_J4",
    authDomain: "todo-vue1-proj.firebaseapp.com",
    databaseURL: "https://todo-vue1-proj.firebaseio.com",
    projectId: "todo-vue1-proj",
    storageBucket: "todo-vue1-proj.appspot.com",
    messagingSenderId: "123679623527",
}
firebase.initializeApp(firebaseConfig)
const db = firebase.firestore()

db.settings({ timestampsInSnapshots: true})

export default db
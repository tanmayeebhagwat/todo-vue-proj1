module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true,
        "node": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:vue/essential"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018
    },
    "plugins": [
        "vue"
    ],
    "rules": {
        "no-console":0,
        "semi": ["error", "never"],
        "no-trailing-spaces": 2,
        "no-var": 2,
        "prefer-const": 2,
        "quotes": ["error", "double", { "avoidEscape": true }],
        "keyword-spacing": ["error", {"before": true, "after":true}],
        "space-before-function-paren": ["error", "always"],
        "no-multi-spaces": 2
    }
}